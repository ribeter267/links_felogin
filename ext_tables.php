<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

$TCA['tx_linksfelogin'] = array(
	'ctrl' => array(
		'title'     => 'LLL:EXT:links_felogin/locallang_db.xml:tx_linksfelogin',		
		'label'     => 'title',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'rootLevel' => 1,
		'default_sortby' => 'ORDER BY crdate',	
		'delete' => 'deleted',	
		'enablecolumns' => array(		
			'disabled' => 'hidden',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY) . 'icon_tx_linksfelogin.gif',
	),
);


$tempColumns = array(
	'tx_linksfelogin_extlinks' => array(		
		'exclude' => 0,		
		'label' => 'LLL:EXT:links_felogin/locallang_db.xml:fe_groups.tx_linksfelogin_extlinks',		
		'config' => array(
			'type' => 'select',	
			'foreign_table' => 'tx_linksfelogin',	
			'foreign_table_where' => 'AND tx_linksfelogin.pid=###SITEROOT### AND tx_linksfelogin.hidden = 0 AND tx_linksfelogin.deleted = 0 ORDER BY tx_linksfelogin.uid',	
			'size' => 10,	
			'minitems' => 0,
			'maxitems' => 999,	
			'wizards' => array(
				'_PADDING'  => 2,
				'_VERTICAL' => 1,
				'add' => array(
					'type'   => 'script',
					'title'  => 'Create new record',
					'icon'   => 'add.gif',
					'params' => array(
						'table'    => 'tx_linksfelogin',
						'pid'      => '###SITEROOT###',
						'setValue' => 'prepend'
					),
					'script' => 'wizard_add.php',
				),
				'list' => array(
					'type'   => 'script',
					'title'  => 'List',
					'icon'   => 'list.gif',
					'params' => array(
						'table' => 'tx_linksfelogin',
						'pid'   => '###SITEROOT###',
					),
					'script' => 'wizard_list.php',
				),
			),
		)
	),
	
);

t3lib_div::loadTCA('fe_groups');
t3lib_extMgm::addTCAcolumns('fe_groups',$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes('fe_groups','tx_linksfelogin_extlinks;;;;1-1-1','','before:subgroup');
?>