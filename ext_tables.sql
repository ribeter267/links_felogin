#
# Table structure for table 'tx_linksfelogin'
#
CREATE TABLE tx_linksfelogin (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	title tinytext,
	link tinytext,
	target varchar(6) DEFAULT '' NOT NULL,
	
	PRIMARY KEY (uid),
	KEY parent (pid)
);
#
# Table structure for table 'fe_groups'
#
CREATE TABLE fe_groups (
	tx_linksfelogin_extlinks text
);