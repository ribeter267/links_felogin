<?php
namespace TaoJiang\LinksFelogin\Xclass;
class FrontendLoginController extends \TYPO3\CMS\Felogin\Controller\FrontendLoginController{

	
	/**
	 * Shows logout form
	 *
	 * @return	string		The content.
	 */
	protected function showLogout() {

		$subpartArray = $linkpartArray = array();
		$subpart = $this->cObj->getSubpart($this->template, '###TEMPLATE_LOGOUT###');
	
		if($this->conf['tx_linksfelogin.']['usesoure'] == 1){
		
			$templateFile = $this->conf['tx_linksfelogin.']['templateFile'] ? $this->conf['tx_linksfelogin.']['templateFile'] : 'EXT:tx_linksfelogin/template.html';
			$template = $this->cObj->fileResource($templateFile);
			$subpart = $this->cObj->getSubpart($template, '###TEMPLATE_LOGOUT###');
			$markerArray['###SF###'] = htmlspecialchars($this->getUserGroup());
			$markerArray['###SF_LABEL###'] = $this->pi_getLL('sf', '', 1) ? $this->pi_getLL('sf', '', 1) :  '身份:';
			$markerArray['###USER_LINKS###'] = $this->getUserLinks();
		}
 
		$markerArray['###STATUS_HEADER###'] = $this->getDisplayText('status_header',$this->conf['logoutHeader_stdWrap.']);
		$markerArray['###STATUS_MESSAGE###'] = $this->getDisplayText('status_message', $this->conf['logoutMessage_stdWrap.']);
		$this->cObj->stdWrap($this->flexFormValue('message', 's_status'), $this->conf['logoutMessage_stdWrap.']);

		$markerArray['###LEGEND###'] = $this->pi_getLL('logout', '', 1);
		$markerArray['###ACTION_URI###'] = $this->getPageLink('',array(),TRUE);
		$markerArray['###LOGOUT_LABEL###'] = $this->pi_getLL('logout', '', 1);
		$markerArray['###NAME###'] = htmlspecialchars($GLOBALS['TSFE']->fe_user->user['name']);
		$markerArray['###STORAGE_PID###'] = $this->spid;
		$markerArray['###USERNAME###'] = htmlspecialchars($GLOBALS['TSFE']->fe_user->user['username']);
		$markerArray['###USERNAME_LABEL###'] = $this->pi_getLL('username', '', 1);
		
		$markerArray['###FULLNAME###'] = htmlspecialchars($GLOBALS['TSFE']->fe_user->user['stu_name']);	//stu_name
		
		$markerArray['###NOREDIRECT###'] = $this->noRedirect ? '1' : '0';
		$markerArray['###PREFIXID###'] = $this->prefixId;
		$markerArray = array_merge($markerArray, $this->getUserFieldMarkers());

		if ($this->redirectUrl) {
				// use redirectUrl for action tag because of possible access restricted pages
			$markerArray['###ACTION_URI###'] = htmlspecialchars($this->redirectUrl);
			$this->redirectUrl = '';
		}
		return $this->cObj->substituteMarkerArrayCached($subpart, $markerArray, $subpartArray, $linkpartArray);
	}
	
	
	
	/**
	 * 得到用户组 用户组就是当前用户身份
	 */
	protected function getUserGroup(){
	
		$content = '';
		if(isset($GLOBALS['TSFE']->fe_user->groupData['title'])){
			foreach($GLOBALS['TSFE']->fe_user->groupData['title'] as $g){
				$content .= ' '.$g;
			}
		}
		return $content;
	}
	
	protected function getUserLinks(){
	
		if(!isset($GLOBALS['TSFE']->fe_user->groupData['uid'])) return false;
		
		$content = '';
		$link_ids = '';
		$ids = implode(',',$GLOBALS['TSFE']->fe_user->groupData['uid']);
		$extlink_ids = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tx_linksfelogin_extlinks','fe_groups','uid IN('.$ids.')');
		
		if(!$extlink_ids) return false;
		$i = 0;
		foreach($extlink_ids as $d) {
			$m = ($i == 0)  ? '' : ',';
			$link_ids .= $m.$d['tx_linksfelogin_extlinks'];
			$i++;
		}
		
		$extlinks = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*','tx_linksfelogin','uid IN('.$link_ids.') AND deleted = 0 AND hidden = 0');
		//print_r($link_ids);
		if($extlinks){
			foreach($extlinks as $d){
				$content .= '<span><a href="'.$this->getFormartUrl($d['link']).'" target="'.$d['target'].'">'.$d['title'].'</a></span>';
			}
			
			return $content;
		}
	}
	
	
	
	/**
	 * 得到格式化的URL
	 */
	protected function getFormartUrl($url){
	
		if (preg_match ('/^http:\/\//',$url) || preg_match ('/^https:\/\//',$url)) {
		   return  $url;
		}else if(preg_match ('/^[A-Za-z0-9]*$/',$url)){
			return 'index.php?id='.$url;
		}else{
			return  'http://'.$url;
		}
	}
}