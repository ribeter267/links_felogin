<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');



if (isset($TYPO3_CONF_VARS['SYS']['compat_version'])
    && t3lib_div::int_from_ver($TYPO3_CONF_VARS['SYS']['compat_version']) >= 6002000
) {

	$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Felogin\\Controller\\FrontendLoginController'] = array(
		'className' => 'TaoJiang\\LinksFelogin\\Xclass\\FrontendLoginController'
	);

	$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['tx_felogin_pi1'] = $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Felogin\\Controller\\FrontendLoginController'];

} else {

	$TYPO3_CONF_VARS['FE']['XCLASS']['ext/felogin/pi1/class.tx_felogin_pi1.php'] = t3lib_extMgm::extPath($_EXTKEY).'class.ux_tx_felogin_pi1.php';
}

?>