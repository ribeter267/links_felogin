<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_linksfelogin'] = array(
	'ctrl' => $TCA['tx_linksfelogin']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'hidden,title,link,target'
	),
	'feInterface' => $TCA['tx_linksfelogin']['feInterface'],
	'columns' => array(
		'hidden' => array(		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array(
				'type'    => 'check',
				'default' => '0'
			)
		),
		'title' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:links_felogin/locallang_db.xml:tx_linksfelogin.title',		
			'config' => array(
				'type' => 'input',	
				'size' => '30',
			)
		),
		'link' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:links_felogin/locallang_db.xml:tx_linksfelogin.link',		
			'config' => array(
				'type'     => 'input',
				'size'     => '15',
				'max'      => '255',
				'checkbox' => '',
				'eval'     => 'trim',
				'wizards'  => array(
					'_PADDING' => 2,
					'link'     => array(
						'type'         => 'popup',
						'title'        => 'Link',
						'icon'         => 'link_popup.gif',
						'script'       => 'browse_links.php?mode=wizard',
						'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1'
					)
				)
			)
		),
		'target' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:links_felogin/locallang_db.xml:tx_linksfelogin.target',		
			'config' => array(
				'type' => 'radio',
				'items' => array(
					array('LLL:EXT:links_felogin/locallang_db.xml:tx_linksfelogin.target.I.0', '_self'),
					array('LLL:EXT:links_felogin/locallang_db.xml:tx_linksfelogin.target.I.1', '_blank'),
				),
			)
		),
	),
	'types' => array(
		'0' => array('showitem' => 'hidden;;1;;1-1-1, title;;;;2-2-2, link;;;;3-3-3, target')
	),
	'palettes' => array(
		'1' => array('showitem' => '')
	)
);
?>